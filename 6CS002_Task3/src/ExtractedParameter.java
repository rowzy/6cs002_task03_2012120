import java.awt.Graphics;

public class ExtractedParameter {
	public Graphics g;
	public int x;
	public int y;
	public int diameter;
	public int n;

	public ExtractedParameter(Graphics g, int x, int y, int diameter, int n) {
		this.g = g;
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		this.n = n;
	}
}