import java.util.List;

public class AardvarkData {
	public String playerName;
	public List<Domino> _d;
	public List<Domino> _g;
	public int[][] grid;
	public int[][] gg;
	public int mode;
	public int cf;
	public int score;
	public long startTime;
	public PictureFrame pf;
	public int ZERO;

	public AardvarkData(int[][] grid, int[][] gg, int mode, PictureFrame pf, int zERO) {
		this.grid = grid;
		this.gg = gg;
		this.mode = mode;
		this.pf = pf;
		ZERO = zERO;
	}
}