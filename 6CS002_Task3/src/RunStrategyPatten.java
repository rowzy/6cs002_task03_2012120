
public class RunStrategyPatten {
	public static void main(String[] args) {
	      Context context = new Context(new OprtnAutoPlay1());		
	      context.executeStrategy();

	      context = new Context(new OprtnAutoPlay2());		
	      context.executeStrategy();

	      context = new Context(new OprtnAutoPlay3());		
	      context.executeStrategy();
	   }

}
