
public class MenuView {
	public void displayGUIView() {
		  Subject subject = new Subject();

	      new WelcomeObserver(subject);
	      new PNameObserver(subject);
	      new MainObserver(subject);
	      new DifficultyObserver(subject);
	      
	      subject.notifyAllObservers();
	}

}
