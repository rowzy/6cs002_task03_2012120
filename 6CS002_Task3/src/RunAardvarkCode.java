
public class RunAardvarkCode {
	public static void main(String[] args) {
		GameMenu model = new GameMenu();
		MenuView view = new MenuView();
		MenuControll controller = new MenuControll(model, view);
		controller.updateView();
	}

}
