
public class Context {
	private StrategyGame strategy;

	   public Context(StrategyGame strategy){
	      this.strategy = strategy;
	   }

	   public void executeStrategy(){
	      strategy.autoPlay();
	   }

}
