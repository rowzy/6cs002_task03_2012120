
import java.awt.GridLayout;

import javax.swing.JLabel;

public class WelcomeObserver extends Observer{

   public WelcomeObserver(Subject subject){
      this.subject = subject;
      this.subject.attach(this);
   }
		
   @Override
   public void displayFrame() {
		welcomeFrame.setLayout(new GridLayout(0, 1));
		label = new JLabel(" Welcome To Abominodo Dominoes Puzzle Game");
		welcomeFrame.add(label);
		label = new JLabel(" Version 1.0 (c), New Puzzle Creator, 2022");
		welcomeFrame.add(label);
		welcomeFrame.pack();
		welcomeFrame.setVisible(true);
   }
   
}
