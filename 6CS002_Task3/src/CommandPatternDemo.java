
public class CommandPatternDemo {
	public static void main(String[] args) {
		Aardvark ardvark = new Aardvark(1);
		
		PlaceDominoCommand placeDomino = new PlaceDominoCommand(ardvark);
		
		CommandInvoker commandInvoker = new CommandInvoker(placeDomino);
		commandInvoker.doPlaceDomino();
		
		System.out.println("\nUndo the domino placement (Y/N)?");
		String undo = IOLibrary.getString();
		if(undo.equalsIgnoreCase("Y")) {
			commandInvoker.undoPlaceDomino();
		}
	}

}
