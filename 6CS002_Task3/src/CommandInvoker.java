
public class CommandInvoker {
private Command placeDomino;
	
	public CommandInvoker(PlaceDominoCommand placeDominoCommand) {
		this.placeDomino = placeDominoCommand;
	}
	
	public void doPlaceDomino() {
		placeDomino.execute();
	}
	
	public void undoPlaceDomino() {
		placeDomino.unexecute();
	}

}
